const express = require('express');
const app = express();
const PORT = 3000;

const students = require('./src/student/routes');

app.get('/', (req, res)=>{
    res.send("Hello");
});

app.use(express.json());

app.use('/students', students);

app.listen(PORT, ()=> {console.log(`Listening to the port ${PORT}`);})




