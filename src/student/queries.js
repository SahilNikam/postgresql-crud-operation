const getStudents = "SELECT * FROM students";
const getStudentByID = "SELECT * FROM students where id = $1";
const checkEmailExist = "SELECT * FROM students where email = $1";
const addStudent = "INSERT INTO students (name, email, age, dob) values ($1, $2, $3, $4)";
const removeStudent = "DELETE from students where id = $1";
const updateStudent = "UPDATE students SET name = $1 where id = $2";

module.exports = {
    getStudents,
    getStudentByID,
    checkEmailExist,
    addStudent,
    removeStudent,
    updateStudent
}