const { Router } = require('express');
const router = Router();
const controller = require('./controller');

router.get('/',controller.getStudents);
router.get('/:id', controller.getStudentByID);
router.post('/', controller.addStudent);
router.delete('/:id', controller.removeStudent);
router.put('/:id', controller.updateStudent);

module.exports = router;