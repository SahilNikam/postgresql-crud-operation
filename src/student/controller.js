const { parse } = require('dotenv');
const pool = require('../../db');
const queries = require('../student/queries');

const getStudents = (req, res)=> {
    pool.query(queries.getStudents, (error, results) => {
        if(error) throw error;
        res.status(200).json(results.rows);
    });
    // res.send("get students");
};

const getStudentByID = (req, res) => {
    const id = parseInt(req.params.id);
    pool.query(queries.getStudentByID,[id], (error, result) => {
        if(error){
            res.send("Given Id is not valid");
        }
        res.status(200).json(result.rows);
    });
};

const addStudent = (req, res) => {
    const {name , email, age, dob} = req.body;
    pool.query(queries.checkEmailExist, [email], (error, result) => {
        if(error) throw error;
        if(result.rows.length){
            res.send("Email. Already Exists");
        }
    });
    pool.query(queries.addStudent, [name, email, age, dob], (error, result) => {
        if(error) throw error;
        res.status(200).send("Student added successfully");
    });
}

const removeStudent = (req, res) => {
    const id = parseInt(req.params.id);
    pool.query(queries.removeStudent, [id], (error, result)=> {
        if(error) throw error;
        res.send("Record is deleted Successfully");
    });
}

const updateStudent = (req, res) => {
    const id = parseInt(req.params.id);
    const { name } = req.body;

    pool.query(queries.getStudentByID,[id], (error, result) => {
        if(error){
            res.send("Given Id is not valid");
        }
    });
    pool.query(queries.updateStudent, [name, id], (error, result) => {
        if(error) throw error;
        res.status(200).send("Student Updated Successfully");
    });
}

module.exports = {
    getStudents,
    getStudentByID,
    addStudent,
    removeStudent,
    updateStudent
};